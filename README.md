# CIGeoE Circumvent Polygon

This plugin identifies dangles in a viewport.

# Installation

For QGIS 3:

There are two methods to install the plugin:

- Method 1:

  1- Copy the folder “cigeoe_circumvent_polygon” to folder:

  ```bash
  [drive]:\Users\[user]\AppData\Roaming\QGIS\QGIS3\profiles\default\python\plugins
  ```

  2 - Start QGis and go to menu "Plugins" and select "Manage and Install Plugins"

  3 - Select “CIGeoE Circumvent Polygon”

  4 - After confirm the operation the plugin will be available in toolbar

- Method 2:

  1 - Compress the folder “cigeoe_circumvent_polygon” (.zip).

  2 - Start QGis and go to "Plugins" and select “Manage and Install Plugins”

  3 - Select “Install from Zip”, choose .zip generated in point 1) and then select "Install Plugin"

# Usage

1 - In the “Layers Panel” select the “layer” (“polygon geometry”), activate the “Select Features” tool and select the polygon to encircle/cut.

![ALT](/images/image1.png)
![ALT](/images/image2.png)

2 - In the “Layers Panel” select the “layer” (“line geometry”), activate the “Select Features” tool if it is not active and select the line that intersects the polygon.

![ALT](/images/image3.png)

3 - Select plugin icon

![ALT](/images/image4.png)

# Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

# License

[AGPL 3.0](https://www.gnu.org/licenses/agpl-3.0.en.html)
